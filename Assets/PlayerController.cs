﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Transform groundTrigger;
    public LayerMask groundLayers;

    private Rigidbody rb;
    private Renderer renderer;

    private float horizontalSpeed = 10.0f;
    private float jumpForce = 700.0f;

    private bool jumpPressed = false;
    private bool canJump = true;
    private bool doubleJumpUsed = false;

    private float jumpDelayRate = 0.1f;
    private float nextJumpTick;

    void Start () {

        rb = gameObject.GetComponent<Rigidbody>();
        renderer = gameObject.GetComponent<Renderer>();

    }

    void Update () {

        if (Input.touchSupported) {

            jumpPressed = Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;

        } else {

            jumpPressed = Input.GetKeyDown(KeyCode.Space);

        }

    }

    void FixedUpdate () {

        Vector2 movement = new Vector2(horizontalSpeed, rb.velocity.y);

        rb.velocity = movement;

        if (Time.time > nextJumpTick) {

            // bool grounded = Physics.OverlapSphere(groundTrigger.position, 0.1f, groundLayers).Length > 0;

            // if (grounded) {

            //     canJump = true;
            //     doubleJumpUsed = false;

            // }

            if (canJump && jumpPressed) {

                rb.velocity = new Vector2(rb.velocity.x, 0);

                rb.AddForce(new Vector2(0, jumpForce));

                if (!doubleJumpUsed) {

                    doubleJumpUsed = true;

                } else {

                    canJump = false;

                }

                nextJumpTick = Time.time + jumpDelayRate;

            }

            if (canJump && !doubleJumpUsed) {

                renderer.material.color = Color.green;

            } else if (canJump && doubleJumpUsed) {

                renderer.material.color = Color.red;

            } else {

                renderer.material.color = Color.white;

            }

        }

    }

    void OnCollisionEnter (Collision other) {

        if (other.gameObject.tag == "Platform") {

            canJump = true;
            doubleJumpUsed = false;

        }

    }

}
